<?php
use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Congratulations!</h1>

        <div class="col-sm-24 col-md-16">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3> Exitos </h3>
                        <p>  Lista de exitos actuales</p>
                        <p>
                            <?= Html::a('Leer mas', ['site/exitos'], ['class' => 'btn btn-warning'])?> 
                          
                        </p>
                        
                    </div>
                </div>
            </div>

    </div>
</div>

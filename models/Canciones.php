<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "canciones".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $duracion
 * @property string|null $f_lanzamiento
 * @property int|null $reproducciones
 *
 * @property Forman[] $formen
 * @property Lanzan[] $lanzans
 */
class Canciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'canciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duracion', 'f_lanzamiento'], 'safe'],
            [['reproducciones'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'duracion' => 'Duracion',
            'f_lanzamiento' => 'F Lanzamiento',
            'reproducciones' => 'Reproducciones',
        ];
    }

    /**
     * Gets query for [[Formen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormen()
    {
        return $this->hasMany(Forman::className(), ['codigocanciones' => 'codigo']);
    }

    /**
     * Gets query for [[Lanzans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLanzans()
    {
        return $this->hasMany(Lanzan::className(), ['codigocanciones' => 'codigo']);
    }
}
